#!/bin/bash
sudo apt update -y -qq
sudo apt upgrade -y -qq
sudo apt install -y -qq apt-utils
sudo apt install -y -qq \
    automake autoconf m4 \
    build-essential \
    software-properties-common \
    vim htop screen vlc \
    apt-transport-https curl \
    ca-certificates gnupg-agent  \
    dnsutils whois zip net-tools \
    sshpass telnet aptitude git \
    zlib1g-dev libssl-dev \
    libreadline-dev libyaml-dev \
    libsqlite3-dev libxml2-dev \
    libxslt1-dev libcurl4-openssl-dev \
    libffi-dev libexpat1-dev \
    gettext unzip \
    libbz2-dev libncurses5-dev \
    xz-utils wget llvm \
    libxmlsec1-dev  liblzma-dev \
    mercurial binutils bison \
    bash-completion

curl -fsSL https://get.docker.com -o /tmp/get-docker.sh
sudo sh /tmp/get-docker.sh
sudo groupadd docker
sudo usermod -aG docker ${USER}
sudo systemctl enable docker
sudo systemctl start docker && sudo systemctl status docker


curl -Ls https://pyenv.run | bash
export PATH="${HOME}/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
echo 'export PATH="${HOME}/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"'>>~/.bashrc
pyenv install 3.8.5
pyenv global  3.8.5

sudo curl https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -sLo /bin/jq && \
    sudo chmod +x /bin/jq

bash < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)
[[ -s "${HOME}/.gvm/scripts/gvm" ]] && source "${HOME}/.gvm/scripts/gvm"
gvm install go1.4 -B
gvm use go1.4
export GOROOT_BOOTSTRAP=${GOROOT}
gvm install go1.14
gvm use go1.14 --default

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"
nvm install --lts

cd /tmp
wget https://github.com/koalaman/shellcheck/releases/download/latest/shellcheck-latest.linux.x86_64.tar.xz
tar xf shellcheck-latest.linux.x86_64.tar.xz
sudo mv shellcheck-latest/shellcheck /bin/
sudo chmod +x /bin/shellcheck

wget https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.deb
sudo apt install -y -qq ./dive_0.9.2_linux_amd64.deb

GO111MODULE=on go get mvdan.cc/sh/v3/cmd/shfmt

curl -sLO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /bin/kubectl
kubectl version --client
echo 'source <(kubectl completion bash)' >>~/.bashrc
echo 'alias k=kubectl' >>~/.bashrc
echo 'complete -F __start_kubectl k' >>~/.bashrc
